// MIT License
//
// Copyright (c) 2018 Magnus Bjerke Vik
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// ==UserScript==
// @name Gitlab Issue Weight Sum
// @namespace https://daidata.net/
// @version 1.1.0
// @description Show a sum of all issue weights present on the Gitlab issue list page
// @author Magnus Bjerke Vik <mbvett@gmail.com>
// @match https://gitlab.com/*/*/issues*
// @grant none
// ==/UserScript==

const issuesListEl = document.getElementsByClassName('issues-list')[0];
let weightSum = 0;
let assigneeMap = new Map();

for (const issueEl of issuesListEl.children) {
  const weightEl = issueEl.getElementsByClassName('issuable-weight')[0];
  if (weightEl) {
    const weightText = weightEl.textContent;
    const weight = parseInt(weightText, 10);
    weightSum += weight;

    const metaEl = issueEl.getElementsByClassName('issuable-meta')[0];
    const avatarEl = metaEl.getElementsByClassName('avatar')[0];
    if (avatarEl) {
      const avatarUrl = avatarEl.getAttribute('data-src');
      let assignee = assigneeMap.get(avatarUrl) || { weight: 0, avatarEl };
      assignee.weight += weight;
      assigneeMap.set(avatarUrl, assignee);
    }
  }
}

let sumEl = document.createElement('div');
sumEl.style.padding = '10px 0 10px 16px';
sumEl.style.borderBottom = '1px solid #eee';
issuesListEl.insertAdjacentElement('beforebegin', sumEl);

let weightIconEl = document.createElement('i');
weightIconEl.className = 'fa fa-balance-scale';
sumEl.insertAdjacentElement('beforeend', weightIconEl);

let sumValueEl = document.createElement('span');
sumValueEl.textContent = weightSum;
sumValueEl.title = 'total weight on issues on this page';
sumValueEl.style.marginLeft = '2px';
sumEl.insertAdjacentElement('beforeend', sumValueEl);

for (const assignee of assigneeMap.values()) {
  let assigneeSumEl = document.createElement('div');
  assigneeSumEl.style.display = 'inline-block';
  assigneeSumEl.style.marginLeft = '10px';
  sumEl.insertAdjacentElement('beforeend', assigneeSumEl);

  assigneeSumEl.insertAdjacentElement('beforeend', assignee.avatarEl);

  let assigneeSumValueEl = document.createElement('span');
  assigneeSumValueEl.textContent = assignee.weight;
  assigneeSumValueEl.title = 'total weight on issues assigned to this person on this page';
  assigneeSumEl.insertAdjacentElement('beforeend', assigneeSumValueEl);
}
